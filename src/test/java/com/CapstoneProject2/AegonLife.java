package com.CapstoneProject2;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AegonLife {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get(
				"https://www.aegonlife.com/fund-performance/nav-rate?field_scheme_tid=3&field_published_date_value%5Bmin%5D%5Bdate%5D=29-09-2021&field_published_date_value%5Bmax%5D%5Bdate%5D=29-09-2021");
		driver.manage().window().maximize();
		
		WebElement schemeDropdown = driver.findElement(By.xpath("//label[text()=' Scheme Name ']//following::select[1]"));
		Select schemeSelect = new Select(schemeDropdown);
		schemeSelect.selectByVisibleText("DEBT FUND");
	
		
		driver.findElement(By.xpath("(//label[text()='Date ']//following-sibling::input)[1]")).click();
		
		WebElement monthSelect = driver.findElement(By.xpath("//label[text()='Start date ']//following::select[2]"));
		Select month = new Select(monthSelect);
		month.selectByVisibleText("Nov");
		
		WebElement yearSelect = driver.findElement(By.xpath("//label[text()='Start date ']//following::select[3]"));
		Select year = new Select(yearSelect);
		year.selectByVisibleText("2019");
		
		driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//following::a[text()='10']")).click();
		
		driver.findElement(By.xpath("(//label[text()='Date ']//following-sibling::input)[2]")).click();
		
		driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//following::a[text()='29']")).click();
		
		driver.findElement(By.xpath("//input[@type='submit' and @value='Search']")).click();
		
		//for getting text using loop
		//h1[text()='NAV Rates']//following::table//child::tbody//child::tr
		
		List<WebElement> navRates = driver.findElements(By.xpath("//h1[text()='NAV Rates']//following::table//child::tbody//child::tr"));
		int rowCount = navRates.size();
		
		List<WebElement> dataRates = driver.findElements(By.xpath("//h1[text()='NAV Rates']//following::table//child::tbody//child::tr//child::td"));
		int dataCount = dataRates.size();
		
		for (int i = 0; i < rowCount; i++) {
			String dataRow = driver.findElement(By.xpath("//h1[text()='NAV Rates']//following::table//child::tbody//child::tr"+i)).getText();
			for (int j = 0; j < dataCount; j++) {
				String data = driver.findElement(By.xpath("//h1[text()='NAV Rates']//following::table//child::tbody//child::tr"+i+"//child::td")).getText();
				System.out.println(data);
			}
			
		}
	}


	}


